<?php declare(strict_types=1);

namespace Housing\Infrastructure\Comparer;

use Collections\Generic\ComparerInterface as BaseComparerInterface;

interface ComparerInterface extends BaseComparerInterface
{
    /**
     * @return string
     */
    public function getName() : string;
}
