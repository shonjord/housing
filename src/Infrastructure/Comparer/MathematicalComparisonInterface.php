<?php declare(strict_types=1);

namespace Housing\Infrastructure\Comparer;

/**
 * Responsible to compare X & Y values with the most basic mathematical comparisons
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface MathematicalComparisonInterface
{
    /**
     * Responsible for comparing two values
     *
     * @param $x
     * @param $y
     *
     * @return int
     */
    public function getComparedAscValue($x, $y) : int;

    /**
     * @param string $x
     * @param string $y
     *
     * @return int
     */
    public function getComparedDescValue($x, $y) : int;

    /**
     * Comparing if X value is higher than Y
     *
     * @param $x
     * @param $y
     *
     * @return bool
     */
    public function xIsHigherThanY($x, $y) : bool;

    /**
     * @param string $x
     * @param string $y
     *
     * @return bool
     */
    public function xIsLessThanY($x, $y) : bool;

    /**
     * Comparing if X is equal to Y
     *
     * @param $x
     * @param $y
     *
     * @return bool
     */
    public function xIsEqualsToY($x, $y) : bool;
}
