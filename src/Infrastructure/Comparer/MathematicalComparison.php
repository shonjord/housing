<?php declare(strict_types=1);

namespace Housing\Infrastructure\Comparer;

/**
 * Responsible to compare X & Y values with the most basic mathematical comparisons
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class MathematicalComparison implements MathematicalComparisonInterface
{
    /**
     * Responsible for comparing two values
     *
     * @param $x
     * @param $y
     *
     * @return int
     */
    public function getComparedAscValue($x, $y) : int
    {
        if ($this->xIsHigherThanY($x, $y)) {
            return 1;
        }

        if ($this->xIsEqualsToY($x, $y)) {
            return 0;
        }

        return -1;
    }

    /**
     * @param string $x
     * @param string $y
     *
     * @return int
     */
    public function getComparedDescValue($x, $y) : int
    {
        if ($this->xIsLessThanY($x, $y)) {
            return 1;
        }

        if ($this->xIsEqualsToY($x, $y)) {
            return 0;
        }

        return -1;
    }

    /**
     * Comparing if X value is higher than Y
     *
     * @param $x
     * @param $y
     *
     * @return bool
     */
    public function xIsHigherThanY($x, $y) : bool
    {
        return $x > $y;
    }

    /**
     * @param string $x
     * @param string $y
     *
     * @return bool
     */
    public function xIsLessThanY($x, $y) : bool
    {
        return $x < $y;
    }

    /**
     * Comparing if X is equal to Y
     *
     * @param $x
     * @param $y
     *
     * @return bool
     */
    public function xIsEqualsToY($x, $y) : bool
    {
        return $x === $y;
    }
}
