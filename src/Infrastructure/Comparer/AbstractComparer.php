<?php declare(strict_types=1);

namespace Housing\Infrastructure\Comparer;

/**
 * Any comparer that extends from this class, will inherit the mathematical comparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class AbstractComparer implements ComparerInterface
{
    /**
     * @var MathematicalComparisonInterface
     */
    protected $comparer;

    /**
     * @param MathematicalComparisonInterface $comparer
     */
    public function __construct(MathematicalComparisonInterface $comparer)
    {
        $this->comparer = $comparer;
    }
}
