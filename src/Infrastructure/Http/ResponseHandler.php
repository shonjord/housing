<?php declare(strict_types=1);

namespace Housing\Infrastructure\Http;

use Collections\MapInterface;
use Housing\Infrastructure\Serializer\SerializerInterface;

final class ResponseHandler implements ResponseHandlerInterface
{
    private const ERROR     = '{"Error": "%s"}';
    private const APP_JSON  = [ 'Content-Type' => 'application/json' ];

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param MapInterface $content
     * @param int $response
     * @return ResponseInterface
     */
    public function jsonResponse(MapInterface $content, int $response = Response::HTTP_OK) : ResponseInterface
    {
        return new Response($this->serializer->serialize($content), $response, self::APP_JSON);
    }

    /**
     * @param \Exception $exception
     * @return ResponseInterface
     */
    public function exceptionResponse(\Exception $exception) : ResponseInterface
    {
        return new Response(sprintf(self::ERROR, $exception->getMessage()), Response::HTTP_BAD_REQUEST, self::APP_JSON);
    }
}
