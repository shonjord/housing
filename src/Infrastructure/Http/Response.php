<?php declare(strict_types=1);

namespace Housing\Infrastructure\Http;

use Symfony\Component\HttpFoundation\Response as SilexResponse;

final class Response extends SilexResponse implements ResponseInterface
{

}
