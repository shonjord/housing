<?php declare(strict_types=1);

namespace Housing\Infrastructure\Http;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request as SilexRequest;

final class Request implements RequestInterface
{

    /**
     * @var SilexRequest
     */
    private $request;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator() : \ArrayIterator
    {
        return $this->request->query->getIterator();
    }

    /**
     * @return null|string
     */
    public function getCurrentIterator() : ?string
    {
        return $this->getIterator()->current();
    }


    /**
     * @return null|string
     */
    public function getIteratorKey() : ?string
    {
        return $this->getIterator()->key();
    }
}