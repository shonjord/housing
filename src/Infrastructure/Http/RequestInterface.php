<?php declare(strict_types=1);

namespace Housing\Infrastructure\Http;

interface RequestInterface
{
    /**
     * @return \ArrayIterator
     */
    public function getIterator() : \ArrayIterator;

    /**
     * @return null|string
     */
    public function getCurrentIterator() : ?string;


    /**
     * @return null|string
     */
    public function getIteratorKey() : ?string;
}
