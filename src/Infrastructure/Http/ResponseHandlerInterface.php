<?php

namespace Housing\Infrastructure\Http;

use Collections\MapInterface;

interface ResponseHandlerInterface
{
    /**
     * @param MapInterface $content
     * @return ResponseInterface
     */
    public function jsonResponse(MapInterface $content) : ResponseInterface;

    /**
     * @param \Exception $exception
     * @return ResponseInterface
     */
    public function exceptionResponse(\Exception $exception) : ResponseInterface;
}
