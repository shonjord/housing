<?php declare(strict_types=1);

namespace Housing\Infrastructure\Repository;

use Collections\Map;
use Housing\Domain\Catalog\Entity\Catalog;
use Housing\Infrastructure\Reader\ReaderInterface;
use Housing\Infrastructure\Serializer\SerializerInterface;

/**
 * Responsible to deserialize JSON data to PHP objects and filter by City {ID, name}
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class AbstractRepository
{
    private const NO_RESULT = 'No result found for: %s';

    /**
     * @var ReaderInterface
     */
    protected $reader;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param ReaderInterface $reader
     * @param SerializerInterface $serializer
     */
    public function __construct(ReaderInterface $reader, SerializerInterface $serializer)
    {
        $this->reader     = $reader;
        $this->serializer = $serializer;
    }

    /**
     * @param string $value
     * @return Catalog
     * @throws \Exception
     */
    protected function findBy(string $value) : Catalog
    {
        $result = $this->getCollection()->filter(function (Catalog $catalog) use ($value) : bool {
            return $catalog->has($value);
        });

        if ($result->isEmpty()) {
            throw new \Exception(sprintf(self::NO_RESULT, $value));
        }

        return $result->first();
    }

    /**
     * @return Map
     */
    protected function getCollection() : Map
    {
        return $this->reader->map(function (Map $data) : Catalog {
            return $this->serializer->deserialize($data, Catalog::class);
        });
    }
}
