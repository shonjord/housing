<?php declare(strict_types=1);

namespace Housing\Infrastructure\Reader;

use Collections\Map;
use Collections\MapInterface;

/**
 * Returns content
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Content
{
    /**
     * @var string
     */
    private $content;

    /**
     * @param string|array|\stdClass $content
     */
    private function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * @param array $content
     * @return Content
     */
    public static function fromArray(array $content) : Content
    {
        return new self($content);
    }

    /**
     * @param string $content
     * @return Content
     */
    public static function fromString(string $content) : Content
    {
        return new self($content);
    }

    /**
     * @param \stdClass $content
     * @return Content
     */
    public static function fromObject(\stdClass $content) : Content
    {
        return new self($content);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return is_array($this->content) ? $this->content : [$this->content];
    }

    /**
     * @return MapInterface
     */
    public function toMap() : MapInterface
    {
        return new Map($this->toArray());
    }

    /**
     * @param callable $callable
     * @return MapInterface
     */
    public function map(callable $callable) : MapInterface
    {
        return $this->toMap()->map($callable);
    }
}
