<?php declare(strict_types=1);

namespace Housing\Infrastructure\Reader;

use Collections\MapInterface;
use Housing\Infrastructure\Reader\Exception\CantReadFileException;

/**
 * Reads json data from local file or remote
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Reader implements ReaderInterface
{
    /**
     * @var string|array|\stdClass
     */
    private $content;

    /**
     * @var string
     */
    private $file;

    /**
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file    = $file;
        $this->content = $this->getValidContent();
    }

    /**
     * @return string
     */
    public function getFile() : string
    {
        return $this->file;
    }

    /**
     * @return Content
     */
    public function getContent() : Content
    {
        return Content::fromArray($this->content);
    }

    /**
     * @param callable $callable
     * @return MapInterface
     */
    public function map(callable $callable) : MapInterface
    {
        return $this->getContent()->map($callable);
    }

    /**
     * @return array
     * @throws CantReadFileException
     */
    private function getValidContent() : array
    {
        try {
            $content = file_get_contents($this->file);
        } catch (\Exception $exception) {
            throw new CantReadFileException(
                sprintf('File or Directory can\'t be read: %s', $this->file)
            );
        }

        $validContent = json_decode($content, true);

        if (null === $validContent) {
            throw new CantReadFileException(
                sprintf('File is not a readable JSON: %s', $this->file)
            );
        }
        return $validContent;
    }
}
