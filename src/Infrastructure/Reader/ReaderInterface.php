<?php declare(strict_types=1);

namespace Housing\Infrastructure\Reader;

use Collections\MapInterface;

/**
 * Reads json data from local file or remote
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface ReaderInterface
{
    /**
     * @return string
     */
    public function getFile() : string;

    /**
     * @return Content
     */
    public function getContent() : Content;

    /**
     * @param callable $callable
     * @return MapInterface
     */
    public function map(callable $callable) : MapInterface;
}
