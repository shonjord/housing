<?php declare(strict_types=1);

namespace Housing\Infrastructure\Reader\Exception;

/**
 * Exception to thrown when a file can't be read
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class CantReadFileException extends \Exception
{
    /**
     * @var string
     */
    public $message = 'Can\t read the file';
}
