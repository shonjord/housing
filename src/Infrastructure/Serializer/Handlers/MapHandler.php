<?php declare(strict_types=1);

namespace Housing\Infrastructure\Serializer\Handlers;

use Collections\Map;
use JMS\Serializer\Context;
use Collections\MapInterface;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;

/**
 * Handles collection (Map) object serialization process
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class MapHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'Map',
                'method'    => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format'    => 'json',
                'type'      => 'Map',
                'method'    => 'deserialize'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param MapInterface $map
     * @param array $type
     * @param Context $context
     * @return array
     */
    public function serialize(VisitorInterface $visitor, MapInterface $map, array $type, Context $context) : array
    {
        return $visitor->visitArray($map->toArray(), $type, $context);
    }

    /**
     * @param VisitorInterface $visitor
     * @param array $data
     * @param array $type
     * @param Context $context
     * @return MapInterface
     */
    public function deserialize(VisitorInterface $visitor, array $data, array $type, Context $context) : MapInterface
    {
        return new Map(
            $visitor->visitArray($data, $type, $context)
        );
    }
}

