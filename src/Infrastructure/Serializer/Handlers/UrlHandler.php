<?php declare(strict_types=1);

namespace Housing\Infrastructure\Serializer\Handlers;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;
use Housing\Domain\Partner\ValueObject\Url;
use JMS\Serializer\Handler\SubscribingHandlerInterface;

/**
 * Handles URL value object serialization process
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class UrlHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods() : array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'Url',
                'method'    => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format'    => 'json',
                'type'      => 'Url',
                'method'    => 'deserialize'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param Url $url
     * @param array $type
     * @param Context $context
     * @return string
     */
    public function serialize(VisitorInterface $visitor, Url $url, array $type, Context $context) : string
    {
        return $visitor->visitString((string) $url, $type, $context);
    }

    /**
     * @param VisitorInterface $visitor
     * @param string $url
     * @param array $type
     * @param Context $context
     * @return Url
     * @throws \Exception
     */
    public function deserialize(VisitorInterface $visitor, string $url, array $type, Context $context) : Url
    {
        try {
            return new Url($visitor->visitString($url, $type, $context));
        } catch (\Exception $badUrlException) {
            throw $badUrlException;
        }
    }
}
