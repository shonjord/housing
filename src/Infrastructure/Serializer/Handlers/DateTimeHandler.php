<?php declare(strict_types=1);

namespace Housing\Infrastructure\Serializer\Handlers;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;

/**
 * Handles DateTime object serialization process
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class DateTimeHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods() : array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'DateTime',
                'method'    => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format'    => 'json',
                'type'      => 'DateTime',
                'method'    => 'deserialize'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param \DateTime $date
     * @param array $type
     * @param Context $context
     * @return string
     */
    public function serialize(VisitorInterface $visitor, \DateTime $date, array $type, Context $context) : string
    {
        return $visitor->visitString($date->format(current($type['params'])), $type, $context);
    }

    /**
     * @param VisitorInterface $visitor
     * @param string $date
     * @param array $type
     * @param Context $context
     * @return \DateTime
     */
    public function deserialize(VisitorInterface $visitor, string $date, array $type, Context $context) : \DateTime
    {
        return new \DateTime(
            $visitor->visitString($date, $type, $context)
        );
    }
}
