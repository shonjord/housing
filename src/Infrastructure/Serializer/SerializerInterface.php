<?php declare(strict_types=1);

namespace Housing\Infrastructure\Serializer;

/**
 * Serialize|Deserialize data
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface SerializerInterface
{
    /**
     * @param $data
     * @param $entity
     * @return array|mixed|object
     */
    public function deserialize($data, $entity);

    /**
     * @param $data
     * @return mixed|string
     */
    public function serialize($data);
}
