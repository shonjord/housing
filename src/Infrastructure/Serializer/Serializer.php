<?php declare(strict_types = 1);

namespace Housing\Infrastructure\Serializer;

use Collections\MapInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Serializer as SerializerLibrary;
use Housing\Infrastructure\Serializer\Handlers\UrlHandler;
use Housing\Infrastructure\Serializer\Handlers\MapHandler;
use Housing\Infrastructure\Serializer\Handlers\DateTimeHandler;

/**
 * Serialize|Deserialize data using JMS library
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Serializer implements SerializerInterface
{
    /**
     * @var SerializerLibrary
     */
    private $serializer;

    public function __construct()
    {
        $this->serializer = $this->getSerializer();
    }

    /**
     * @param string $data
     * @param $entity
     * @return array|mixed|object
     */
    public function deserialize($data, $entity)
    {
        return $this->serializer->deserialize($this->serialize($data), $entity, 'json');
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function serialize($data)
    {
        return $this->serializer->serialize($data instanceof MapInterface ? $data->toArray() : $data, 'json');
    }

    /**
     * @return SerializerLibrary
     */
    private function getSerializer() : SerializerLibrary
    {
        return SerializerBuilder::create()->addMetadataDir(getenv('SERIALIZER_FOLDER'))
                                          ->configureHandlers($this->getHandlers())
                                          ->build();
    }
    
    /**
     * @return \Closure
     */
    private function getHandlers() : \Closure
    {
        return function (HandlerRegistry $registry) : void {
            $registry->registerSubscribingHandler(new MapHandler);
            $registry->registerSubscribingHandler(new UrlHandler);
            $registry->registerSubscribingHandler(new DateTimeHandler);
        };
    }
}
