<?php declare(strict_types=1);

namespace Housing\Domain\Partner\Comparer;

use Housing\Domain\Partner\Entity\Partner;
use Housing\Infrastructure\Comparer\AbstractComparer;

/**
 * Checks if Partner URL A > URL B and sort it ascending
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerUrlComparer extends AbstractComparer
{
    /**
     * Compares two objects and returns a value indicating whether one is less than, equal to, or greater
     * than the other.
     * @param Partner $first The first object to compare.
     * @param Partner $second The second object to compare.
     * @return int A int that indicates the relative values of x and y, as shown in the following table.
     */
    public function compare($first, $second) : int
    {
        return $this->comparer->getComparedAscValue($first->getUrl(), $second->getUrl());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'partner_url';
    }
}
