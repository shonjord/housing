<?php declare(strict_types = 1);

namespace Housing\Domain\Partner\ValueObject;

use Housing\Domain\Partner\Exception\BadUrlException;

/**
 * Represents a single URL value object for the Partner
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Url
{
    /**
     * @const int
     */
    private const PORT = 80;

    /**
     * @const int
     */
    private const TIMEOUT = 6;

    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     * @param bool $isValidated
     */
    public function __construct(string $value, bool $isValidated = false)
    {
        $this->value = $isValidated ? $value : $this->valid($value);
    }

    /**
     * @param string $value
     * @return Url
     */
    public static function validated(string $value) : Url
    {
        return new self($value, true);
    }

    /**
     * @param Url $url
     * @return bool
     */
    public function equals(Url $url) : bool
    {
        return (string) $this === (string) $url;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @throws BadUrlException
     * @return string
     */
    private function valid(string $value) : string
    {
        try {
            fsockopen(parse_url($value)['host'], Url::PORT, $errorNumber, $errorString, Url::TIMEOUT);
        } catch (\Exception $exception) {
            throw new BadUrlException(sprintf('URL does not exist: %s', $value));
        }
        return $value;
    }
}
