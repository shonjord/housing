<?php declare(strict_types=1);

namespace Housing\Domain\Partner\Service;

use Collections\Map;
use Collections\MapInterface;
use Housing\Domain\Hotel\Entity\Hotel;
use Housing\Domain\Hotel\Service\AbstractSortableHotelService;

/**
 * Returns sorted Hotels ~> Partners based on the given comparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerOrderedHotelService extends AbstractSortableHotelService
{
    /**
     * @param Map $hotels
     * @return MapInterface
     */
    protected function sorted(Map $hotels) : MapInterface
    {
        return $hotels->each(function (Hotel $hotel) : void {
            $hotel->getPartners()->sort($this->comparer);
        });
    }
}
