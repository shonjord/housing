<?php declare(strict_types = 1);

namespace Housing\Domain\Partner\Entity;

use Collections\Map;
use Collections\MapInterface;
use Housing\Domain\Partner\ValueObject\Url;
use Housing\Domain\Price\Entity\PriceCollection;

/**
 * Represents a single partner from a search result.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Partner
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var array
     */
    private $prices;

    /**
     * @param string $name
     * @param Url $url
     * @param MapInterface $prices
     */
    public function __construct(string $name, Url $url, MapInterface $prices)
    {
        $this->name   = $name;
        $this->url    = $url;
        $this->prices = $prices;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->url;
    }

    /**
     * @return Map
     */
    public function getPrices() : Map
    {
        return $this->prices;
    }

    /**
     * @param MapInterface $collection
     * @return Partner
     */
    public static function fromCollection(MapInterface $collection) : Partner
    {
        $name   = $collection->get('name');
        $url    = Url::validated($collection->get('url'));
        $prices = PriceCollection::fromMap($collection->get('prices'));

        return new self($name, $url, $prices);
    }
}
