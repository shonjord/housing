<?php declare(strict_types=1);

namespace Housing\Domain\Partner\Entity;

use Collections\Map;
use Collections\MapInterface;

final class PartnerCollection extends Map
{
    /**
     * @param MapInterface $collection
     * @return MapInterface
     */
    public static function fromMap(MapInterface $collection)
    {
        return (new self($collection->toArray()))->getPartners();
    }

    /**
     * @return MapInterface
     */
    private function getPartners() : MapInterface
    {
        return $this->map(function (MapInterface $partner) : Partner {
            return Partner::fromCollection($partner);
        });
    }
}
