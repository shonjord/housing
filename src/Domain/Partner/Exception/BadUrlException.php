<?php declare(strict_types = 1);

namespace Housing\Domain\Partner\Exception;

/**
 * Exception to thrown when an URL is incorrect
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
class BadUrlException extends \Exception
{
    /**
     * @var string
     */
    public $message = 'Bad URL encountered';
}
