<?php declare(strict_types=1);

namespace Housing\Domain\Catalog\Entity;

use Collections\Map;
use Collections\MapInterface;

/**
 * Represents the connection between a city {name, ID} and the available hotels
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Catalog
{
    /**
     * @var string
     */
    private $city;

    /**
     * @var int
     */
    private $id;

    /**
     * @var MapInterface
     */
    private $hotels;

    /**
     * @param string $city
     * @param int $id
     * @param MapInterface $hotels
     */
    public function __construct(string $city, int $id, MapInterface $hotels)
    {
        $this->city   = $city;
        $this->id     = $id;
        $this->hotels = $hotels;
    }

    /**
     * @return string
     */
    public function getCity() : string
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return Map
     */
    public function getHotels() : Map
    {
        return $this->hotels;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function has(string $value) : bool
    {
        return is_numeric($value)
            ? $this->getId()   === intval($value)
            : $this->getCity() === ucwords(strtolower($value));
    }
}
