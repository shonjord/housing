<?php declare(strict_types=1);

namespace Housing\Domain\Catalog\Repository;

use Collections\Map;
use Housing\Domain\Catalog\Entity\Catalog;

/**
 * Finds Catalog or Catalogs based on criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface CatalogRepositoryInterface
{
    /**
     * @param string $city
     * @return Catalog
     */
    public function findByCity(string $city) : Catalog;

    /**
     * @return Map
     */
    public function findAll() : Map;
}
