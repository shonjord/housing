<?php declare(strict_types=1);

namespace Housing\Domain\Catalog\Infrastructure;

use Collections\Map;
use Housing\Domain\Catalog\Entity\Catalog;
use Housing\Infrastructure\Repository\AbstractRepository;
use Housing\Domain\Catalog\Repository\CatalogRepositoryInterface;

/**
 * Finds Catalog or Catalogs based on criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class CatalogRepository extends AbstractRepository implements CatalogRepositoryInterface
{
    /**
     * @param string $city
     * @return Catalog
     */
    public function findByCity(string $city): Catalog
    {
        return $this->findBy($city);
    }

    /**
     * @return Map
     */
    public function findAll(): Map
    {
        return $this->getCollection();
    }
}
