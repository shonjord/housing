<?php declare(strict_types=1);

namespace Housing\Domain\Price\Service;

use Collections\Map;
use Collections\MapInterface;
use Housing\Domain\Hotel\Entity\Hotel;
use Housing\Domain\Partner\Entity\Partner;
use Housing\Domain\Hotel\Service\AbstractSortableHotelService;

/**
 * Returns sorted Hotels ~> Partners ~> Prices based on the given comparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceOrderedHotelService extends AbstractSortableHotelService
{
    /**
     * @param Map $hotels
     * @return MapInterface
     */
    protected function sorted(Map $hotels) : MapInterface
    {
        return $hotels->sort($this->comparer)->each(function (Hotel $hotel) : void {
            $hotel->getPartners()->sort($this->comparer)->each(function (Partner $partner) : void {
                $partner->getPrices()->sort($this->comparer);
            });
        });
    }
}
