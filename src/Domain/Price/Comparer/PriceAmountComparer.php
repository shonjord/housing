<?php declare(strict_types=1);

namespace Housing\Domain\Price\Comparer;

use Collections\MapInterface;
use Housing\Domain\Hotel\Entity\Hotel;
use Housing\Domain\Price\Entity\Price;
use Housing\Domain\Partner\Entity\Partner;
use Housing\Infrastructure\Comparer\AbstractComparer;

/**
 * Checks if Price Amount A > Amount B and sort it ascending
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceAmountComparer extends AbstractComparer
{
    /**
     * Compares two objects and returns a value indicating whether one is less than, equal to, or greater
     * than the other.
     * @param Hotel|Partner|Price $first The first object to compare.
     * @param Hotel|Partner|Price $second The second object to compare.
     * @return int A int that indicates the relative values of x and y, as shown in the following table.
     */
    public function compare($first, $second) : int
    {
        $first  = $this->getPriceAmountFrom($first);
        $second = $this->getPriceAmountFrom($second);

        return $this->comparer->getComparedAscValue($first, $second);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'price_amount';
    }

    /**
     * @param Hotel|Partner|Price $entity
     * @return float
     * @throws \Exception
     */
    private function getPriceAmountFrom($entity) : float
    {
        switch (true) {
            case $entity instanceof Hotel:
                return $this->getMinPriceOfPartnersFrom($entity);
            case $entity instanceof Partner:
                return $this->getMinPriceOfPricesFrom($entity);
            case $entity instanceof Price:
                return $entity->getAmount();
        }
        throw new \Exception(
            sprintf('Entity not recognized: %s', (new \ReflectionClass($entity))->getShortName())
        );
    }

    /**
     * @param Hotel $hotel
     * @return float
     */
    private function getMinPriceOfPartnersFrom(Hotel $hotel) : float
    {
        $prices = $hotel->getPartners()->map(function (Partner $partner) : MapInterface {
            return $this->getPartnerPrices($partner);
        });

        $lowestPrices = $prices->map(function (MapInterface $prices) : float {
            return min($prices->toArray());
        });

        return min($lowestPrices->toArray());
    }

    /**
     * @param Partner $partner
     * @return float
     */
    private function getMinPriceOfPricesFrom(Partner $partner) : float
    {
        $prices = $this->getPartnerPrices($partner);

        return min($prices->toArray());
    }

    /**
     * @param Partner $partner
     * @return MapInterface
     */
    private function getPartnerPrices(Partner $partner) : MapInterface
    {
        return $partner->getPrices()->map(function (Price $price) : float {
            return $price->getAmount();
        });
    }
}
