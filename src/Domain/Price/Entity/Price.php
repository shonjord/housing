<?php declare(strict_types = 1);

namespace Housing\Domain\Price\Entity;

use Collections\MapInterface;

/**
 * Represents a single price from a search result related to a single partner.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Price
{
    /**
     * @var string
     */
    public $description;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @param string $description
     * @param float $amount
     * @param \DateTime $from
     * @param \DateTime $to
     */
    public function __construct(string $description, float $amount, \DateTime $from, \DateTime $to)
    {
        $this->description = $description;
        $this->amount      = $amount;
        $this->from        = $from;
        $this->to          = $to;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getAmount() : float
    {
        return $this->amount;
    }

    /**
     * @return \DateTime
     */
    public function getFrom() : \DateTime
    {
        return $this->from;
    }

    /**
     * @return \DateTime
     */
    public function getTo() : \DateTime
    {
        return $this->to;
    }

    /**
     * @param MapInterface $collection
     * @return Price
     */
    public static function fromCollection(MapInterface $collection) : Price
    {
        $description = $collection->get('description');
        $amount      = $collection->get('amount');
        $from        = new \DateTime($collection->get('from'));
        $to          = new \DateTime($collection->get('to'));

        return new self($description, $amount, $from, $to);
    }
}
