<?php

namespace Housing\Domain\Price\Entity;

use Collections\Map;
use Collections\MapInterface;

final class PriceCollection extends Map
{
    /**
     * @param MapInterface $collection
     * @return MapInterface
     */
    public static function fromMap(MapInterface $collection) : MapInterface
    {
        return (new self($collection->toArray()))->getPrices();
    }

    /**
     * @return MapInterface
     */
    private function getPrices() : MapInterface
    {
        return $this->map(function (MapInterface $price) : Price {
            return Price::fromCollection($price);
        });
    }
}
