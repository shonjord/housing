<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Comparer;

use Housing\Domain\Hotel\Entity\Hotel;
use Housing\Infrastructure\Comparer\AbstractComparer;

/**
 * Checks if Hotel Address A > Address B and sort it ascending
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelAddressComparer extends AbstractComparer
{
    /**
     * Compares two objects and returns a value indicating whether one is less than, equal to, or greater
     * than the other.
     * @param Hotel $first The first object to compare.
     * @param Hotel $second The second object to compare.
     * @return int A int that indicates the relative values of x and y, as shown in the following table.
     */
    public function compare($first, $second) : int
    {
        return $this->comparer->getComparedAscValue($first->getAddress(), $second->getAddress());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'hotel_address';
    }
}
