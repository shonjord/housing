<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Entity;

use Collections\Map;
use Collections\MapInterface;
use Housing\Domain\Partner\Entity\PartnerCollection;

/**
 * Represents a single hotel in the result.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Hotel
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var MapInterface
     */
    private $partners;

    /**
     * @param string $name
     * @param string $address
     * @param MapInterface $partners
     */
    public function __construct(string $name, string $address, MapInterface $partners)
    {
        $this->name     = $name;
        $this->address  = $address;
        $this->partners = $partners;
    }

    /**
     * @return string
     */
    public function getName() :string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress() : string
    {
        return $this->address;
    }

    /**
     * @return Map
     */
    public function getPartners() : Map
    {
        return $this->partners;
    }

    /**
     * @param MapInterface $collection
     * @return Hotel
     */
    public static function fromCollection(MapInterface $collection) : Hotel
    {
        $name     = $collection->get('name');
        $address  = $collection->get('address');
        $partners = PartnerCollection::fromMap($collection->get('partners'));

        return new self($name, $address, $partners);
    }
}
