<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Factory;

use Collections\MapInterface;
use Housing\Domain\Hotel\Service\HotelServiceInterface;

/**
 * Responsible to return the correspondent HotelService based on a param criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class ServiceFactory implements ServiceFactoryInterface
{
    /**
     * @var MapInterface
     */
    private $services;

    /**
     * @param MapInterface $services
     */
    public function __construct(MapInterface $services)
    {
        $this->services = $services;
    }

    /**
     * @param null|string $sorting
     * @return HotelServiceInterface
     * @throws QueryIsNoteRecognizedException
     */
    public function create(?string $sorting) : HotelServiceInterface
    {
        return $this->services->at(null === $sorting ? 'unsorted' : strtolower($sorting));
    }
}
