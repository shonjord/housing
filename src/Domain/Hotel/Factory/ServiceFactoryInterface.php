<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Factory;

use Housing\Domain\Hotel\Service\HotelServiceInterface;

/**
 * Responsible to return the correspondent HotelService based on a param criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface ServiceFactoryInterface
{
    /**
     * @param null|string $sorting
     * @return HotelServiceInterface
     */
    public function create(?string $sorting) : HotelServiceInterface;
}
