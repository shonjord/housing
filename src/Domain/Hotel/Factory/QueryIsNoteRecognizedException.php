<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Factory;

final class QueryIsNoteRecognizedException extends \Exception
{
    /**
     * @var string
     */
    public $message = 'Query is not recognized';
}
