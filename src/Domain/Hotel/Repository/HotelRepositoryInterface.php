<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Repository;

use Collections\Map;

/**
 * Finds Hotels based on criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface HotelRepositoryInterface
{
    /**
     * @param string $city
     * @return Map
     */
    public function findByCity(string $city) : Map;
}
