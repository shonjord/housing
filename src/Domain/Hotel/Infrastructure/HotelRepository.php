<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Infrastructure;

use Collections\Map;
use Housing\Infrastructure\Repository\AbstractRepository;
use Housing\Domain\Hotel\Repository\HotelRepositoryInterface;

/**
 * Finds Hotels based on criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelRepository extends AbstractRepository implements HotelRepositoryInterface
{
    /**
     * @param string $city
     * @return Map
     */
    public function findByCity(string $city) : Map
    {
        return $this->findBy($city)->getHotels();
    }
}
