<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Service;

use Collections\Map;
use Collections\MapInterface;

/**
 * Returns sorted Hotels based on the given comparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class OrderedHotelService extends AbstractSortableHotelService
{
    /**
     * @param Map $hotels
     * @return MapInterface
     */
    protected function sorted(Map $hotels) : MapInterface
    {
        return $hotels->sort($this->comparer);
    }
}
