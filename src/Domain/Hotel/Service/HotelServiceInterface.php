<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Service;

use Collections\MapInterface;

/**
 * The implementation is responsible for resolving the id of the city from the
 * given city name or ID.
 *
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
interface HotelServiceInterface
{
    /**
     * @param string $city
     * @return MapInterface
     */
    public function getResultForCity(string $city) : MapInterface;
}
