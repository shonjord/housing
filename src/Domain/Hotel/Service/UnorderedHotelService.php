<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Service;

use Collections\MapInterface;

/**
 * Returns unsorted Hotels
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class UnorderedHotelService extends AbstractHotelService
{
    /**
     * @param string $key
     * @return MapInterface
     */
    protected function cacheAndReturn(string $key) : MapInterface
    {
        $collection = $this->repository->findByCity(
            $this->getCityFrom($key)
        );

        $this->cache->cacheCollection($key, $collection);

        return $this->getCached($key);
    }
}
