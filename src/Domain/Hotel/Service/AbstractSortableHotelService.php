<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Service;

use Collections\Map;
use Collections\MapInterface;

/**
 * Returns a result filtered by ID or name and sorted by the given criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class AbstractSortableHotelService extends AbstractHotelService
{
    /**
     * @param string $key
     * @return MapInterface
     */
    protected function cacheAndReturn(string $key) : MapInterface
    {
        $collection = $this->sorted(
            $this->repository->findByCity($this->getCityFrom($key))
        );

        $this->cache->cacheCollection($key, $collection);

        return $this->getCached($key);
    }

    /**
     * @param Map $hotels
     * @return MapInterface
     */
    abstract protected function sorted(Map $hotels) : MapInterface;
}
