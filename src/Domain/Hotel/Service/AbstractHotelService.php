<?php declare(strict_types=1);

namespace Housing\Domain\Hotel\Service;

use Collections\MapInterface;
use Housing\Domain\Hotel\Entity\Hotel;
use Housing\Application\Cache\CacheServiceInterface as Cache;
use Housing\Infrastructure\Comparer\ComparerInterface as Comparer;
use Housing\Domain\Hotel\Repository\HotelRepositoryInterface as Repository;

/**
 * Encapsulates repository to find hotels by City {ID, name}
 * and if the comparer is injected, its responsibility is to sort out the results
 *
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class AbstractHotelService implements HotelServiceInterface
{
    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @var Comparer|null
     */
    protected $comparer;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @param Repository $repository
     * @param Cache $cache
     * @param Comparer|null $comparer
     */
    public function __construct(Repository $repository, Cache $cache, ?Comparer $comparer)
    {
        $this->repository = $repository;
        $this->cache      = $cache;
        $this->comparer   = $comparer;
    }

    /**
     * @param string $city
     * @return MapInterface
     */
    public function getResultForCity(string $city) : MapInterface
    {
        $key = $this->getKeyFor($city);

        return $this->cache->isCached($key) ? $this->getCached($key) : $this->cacheAndReturn($key);
    }

    /**
     * @param string $key
     * @return MapInterface
     */
    protected function getCached(string $key) : MapInterface
    {
        return $this->cache->getCollection($key)->map(function (MapInterface $collection) : Hotel {
            return Hotel::fromCollection($collection);
        });
    }

    /**
     * @param string $city
     * @return string
     */
    protected function getKeyFor(string $city) : string
    {
        return sprintf("%s-%s", $city, null === $this->comparer ? 'unordered' : $this->comparer->getName());
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getCityFrom(string $key) : string
    {
        return substr($key, 0, strpos($key, '-'));
    }

    /**
     * @param string $city
     * @return MapInterface
     */
    abstract protected function cacheAndReturn(string $city) : MapInterface;
}
