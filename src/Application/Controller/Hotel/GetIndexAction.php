<?php declare(strict_types=1);

namespace Housing\Application\Controller\Hotel;

use Collections\Map;
use Housing\Infrastructure\Http\ResponseInterface;
use Housing\Infrastructure\Http\ResponseHandlerInterface;

/**
 * Responsible to return a JSON response to display welcome
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class GetIndexAction
{
    /**
     * @var ResponseHandlerInterface
     */
    private $response;

    /**
     * @param ResponseHandlerInterface $response
     */
    public function __construct(ResponseHandlerInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return ResponseInterface
     */
    public function __invoke() : ResponseInterface
    {
        return $this->response->jsonResponse(Map::fromArray([
            'Welcome' => sprintf('Housing API v%s', getenv('VERSION'))
        ]));
    }
}
