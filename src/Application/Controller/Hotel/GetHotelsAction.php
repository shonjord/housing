<?php declare(strict_types=1);

namespace Housing\Application\Controller\Hotel;

use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Housing\Infrastructure\Http\RequestInterface as Request;
use Housing\Infrastructure\Http\ResponseInterface as Response;
use Housing\Domain\Hotel\Factory\QueryIsNoteRecognizedException;
use Housing\Domain\Hotel\Factory\ServiceFactoryInterface as ServiceFactory;
use Housing\Infrastructure\Http\ResponseHandlerInterface as ResponseHandler;

/**
 * Responsible to return a JSON response based on sorting criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class GetHotelsAction
{
    /**
     * @var HotelServiceInterface
     */
    private $service;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ResponseHandler
     */
    private $response;


    /**
     * GetHotelsAction constructor.
     * @param Request $request
     * @param ResponseHandler $response
     * @param ServiceFactory $factory
     */
    public function __construct(Request $request, ResponseHandler $response, ServiceFactory $factory)
    {
        $this->request    = $request;
        $this->response   = $response;
        $this->service    = $this->getServiceFrom($factory);
    }

    /**
     * @param string $city
     * @return Response
     */
    public function __invoke(string $city) : Response
    {
        try {
            return $this->response->jsonResponse($this->service->getResultForCity($city));
        } catch (\Exception $exception) {
            return $this->response->exceptionResponse($exception);
        }
    }

    /**
     * @param ServiceFactory $factory
     * @return HotelServiceInterface
     * @throws QueryIsNoteRecognizedException
     */
    private function getServiceFrom(ServiceFactory $factory) : HotelServiceInterface
    {
        $arrayKey = $this->request->getIteratorKey();

        if (null !==  $arrayKey && 'sorted' !== strtolower($arrayKey)) {
            throw new QueryIsNoteRecognizedException();
        }
        return $factory->create($this->request->getCurrentIterator());
    }
}
