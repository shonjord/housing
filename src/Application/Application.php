<?php declare(strict_types=1);

namespace Housing\Application;

use Pimple\ServiceProviderInterface;
use Housing\Application\Services\ServiceContainer;

/**
 * An adapter, responsible to run the entire system
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Application
{
    /**
     * @var Application
     */
    private $application;

    public function __construct()
    {
        $this->application = new \Silex\Application;
    }

    /**
     * @return void
     */
    public function run() : void
    {
        $this->setDebug()
             ->registerServices()
             ->runApplication();
    }

    /**
     * @return Application
     */
    private function setDebug() : Application
    {
        $this->application['debug'] = filter_var(getenv('DEBUG'), FILTER_VALIDATE_BOOLEAN);

        return $this;
    }

    /**
     * @return Application
     */
    private function registerServices() : Application
    {
        ServiceContainer::load()->each(function (ServiceProviderInterface $service) : void {
            $this->application->register($service);
        });

        return $this;
    }

    /**
     * @return void
     */
    private function runApplication() : void
    {
        $this->application->run();
    }
}
