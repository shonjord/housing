<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Housing\Domain\Price\Service\PriceOrderedHotelService;

/**
 * Registers all price services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['sorted.price_amount'] = $this->getSortableWith('amount');
    }

    /**
     * @param string $comparer
     * @return \Closure
     */
    private function getSortableWith(string $comparer) : \Closure
    {
        return function (Container $container) use ($comparer) : HotelServiceInterface {
            return new PriceOrderedHotelService(
                $container['repository.hotel'],
                $container['service.cache'],
                $container["price.{$comparer}"]
            );
        };
    }
}
