<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Service\OrderedHotelService;
use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Housing\Domain\Hotel\Service\UnorderedHotelService;

/**
 * Registers all hotel services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['unsorted.hotel']       = $this->getUnsortedHotel();
        $container['sorted.hotel_name']    = $this->getSortableWith('name');
        $container['sorted.hotel_address'] = $this->getSortableWith('address');
    }

    /**
     * @param string $comparer
     * @return \Closure
     */
    private function getSortableWith(string $comparer) : \Closure
    {
        return function (Container $container) use ($comparer) : HotelServiceInterface {
            return new OrderedHotelService(
                $container['repository.hotel'],
                $container['service.cache'],
                $container["hotel.{$comparer}"]
            );
        };
    }

    /**
     * @return \Closure
     */
    private function getUnsortedHotel() : \Closure
    {
        return function (Container $container) : HotelServiceInterface {
            return new UnorderedHotelService(
                $container['repository.hotel'],
                $container['service.cache'],
                null
            );
        };
    }
}
