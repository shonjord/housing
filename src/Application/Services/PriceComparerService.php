<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Collections\Generic\ComparerInterface;
use Housing\Domain\Price\Comparer\PriceAmountComparer;

/**
 * Registers all price comparer services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceComparerService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['price.amount'] = $this->getPriceComparer();
    }

    /**
     * @return \Closure
     */
    private function getPriceComparer() : \Closure
    {
        return function (Container $container) : ComparerInterface {
            return new PriceAmountComparer($container['comparer']);
        };
    }
}
