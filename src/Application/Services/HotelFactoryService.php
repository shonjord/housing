<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Collections\Map;
use Pimple\Container;
use Collections\MapInterface;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Factory\ServiceFactory;
use Housing\Domain\Hotel\Factory\ServiceFactoryInterface;

/**
 * Registers all factory services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelFactoryService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['service.factory']   = $this->getServiceFactory();
        $container['service.container'] = $this->getServiceContainer();
    }

    /**
     * @return \Closure
     */
    private function getServiceFactory() : \ Closure
    {
        return function (Container $container) : ServiceFactoryInterface {
            return new ServiceFactory($container['service.container']);
        };
    }

    /**
     * @return \Closure
     */
    private function getServiceContainer() : \Closure
    {
        return function (Container $container) : MapInterface {
            return Map::fromArray([
                'unsorted'      => $container['unsorted.hotel'],
                'hotel_name'    => $container['sorted.hotel_name'],
                'hotel_address' => $container['sorted.hotel_address'],
                'partner_name'  => $container['sorted.partner_name'],
                'partner_url'   => $container['sorted.partner_url'],
                'price_amount'  => $container['sorted.price_amount'],
            ]);
        };
    }
}
