<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Collections\Vector;
use Collections\VectorInterface;
use WhoopsPimple\WhoopsServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;

/**
 * Group all services in one Vector.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class ServiceContainer
{
    /**
     * @return VectorInterface
     */
    public static function load() : VectorInterface
    {
        return new Vector([
            new LibraryService,
            new CacheServiceProvider,
            new HttpService,
            new HotelComparerService,
            new PartnerComparerService,
            new PriceComparerService,
            new RepositoryService,
            new RouteService,
            new HotelService,
            new PartnerService,
            new PriceService,
            new HotelFactoryService,
            new ControllerService,
            new WhoopsServiceProvider,
            new ServiceControllerServiceProvider
        ]);
    }
}
