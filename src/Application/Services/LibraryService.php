<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Predis\Client;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Infrastructure\Reader\Reader;
use Housing\Infrastructure\Serializer\Serializer;
use Housing\Infrastructure\Reader\ReaderInterface;
use Housing\Infrastructure\Serializer\SerializerInterface;
use Housing\Infrastructure\Comparer\MathematicalComparison;
use Housing\Infrastructure\Comparer\MathematicalComparisonInterface;

/**
 * Registers all library services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class LibraryService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['serializer'] = $this->getSerializer();
        $container['reader']     = $this->getReader();
        $container['comparer']   = $this->getComparer();
        $container['redis']      = $this->getRedis();

    }

    /**
     * @return \Closure
     */
    private function getSerializer() : \Closure
    {
        return function () : SerializerInterface {
            return new Serializer;
        };
    }

    /**
     * @return \Closure
     */
    private function getReader() : \Closure
    {
        return function () : ReaderInterface {
            return new Reader(getenv('DATA_FILE'));
        };
    }

    /**
     * @return \Closure
     */
    private function getComparer() : \Closure
    {
        return function () : MathematicalComparisonInterface {
            return new MathematicalComparison;
        };
    }

    /**
     * @return \Closure
     */
    private function getRedis() : \Closure
    {
        return function () : Client {
            return new Client([
                'scheme' => getenv('REDIS_SCHEME'),
                'host'   => getenv('REDIS_HOST'),
                'port'   => getenv('REDIS_PORT'),
            ]);
        };
    }
}
