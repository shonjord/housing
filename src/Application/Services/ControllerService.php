<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Application\Controller\Hotel\GetIndexAction;
use Housing\Application\Controller\Hotel\GetHotelsAction;

/**
 * Registers all controller services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class ControllerService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['get_hotels.action'] = $this->getHotelController();
        $container['get_index.action']  = $this->getIndexAction();
    }

    private function getIndexAction() : \Closure
    {
        return function (Container $container) : GetIndexAction {
            return new GetIndexAction($container['service.response']);
        };
    }

    /**
     * @return \Closure
     */
    private function getHotelController() : \Closure
    {
        return function (Container $container) : GetHotelsAction {
            return new GetHotelsAction(
                $container['service.request'],
                $container['service.response'],
                $container['service.factory']
            );
        };
    }
}
