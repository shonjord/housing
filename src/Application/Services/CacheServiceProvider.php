<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Housing\Application\Cache\CacheService;
use Housing\Application\Cache\CacheServiceInterface;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

final class CacheServiceProvider implements ServiceProviderInterface
{

    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        $container['service.cache'] = $this->getCacheService();
    }

    /**
     * @return \Closure
     */
    private function getCacheService() : \Closure
    {
        return function (Container $container) : CacheServiceInterface {
            return new CacheService($container['redis'], $container['serializer']);
        };
    }
}