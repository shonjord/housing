<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Partner\Comparer\PartnerUrlComparer;
use Housing\Domain\Partner\Comparer\PartnerNameComparer;

/**
 * Registers all partner comparer services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
class PartnerComparerService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['partner.name'] = $this->getNameComparer();
        $container['partner.url']  = $this->getUrlComparer();
    }

    /**
     * @return \Closure
     */
    private function getNameComparer() : \Closure
    {
        return function (Container $container) : PartnerNameComparer {
            return new PartnerNameComparer($container['comparer']);
        };
    }

    /**
     * @return \Closure
     */
    private function getUrlComparer() : \Closure
    {
        return function (Container $container) : PartnerUrlComparer {
            return new PartnerUrlComparer($container['comparer']);
        };
    }
}
