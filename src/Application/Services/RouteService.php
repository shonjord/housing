<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Routing\Loader\YamlFileLoader;

/**
 * Registers all route services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class RouteService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['config.location']  = $this->getConfigLocation();
        $container['route.collection'] = $this->getRouteCollection();
        $container['file.loader']      = $this->getFileLoader();

        $container->extend('routes', $this->add($container));

    }

    /**
     * @param Container $container
     * @return \Closure
     */
    private function add(Container $container) : \ Closure
    {
        return function (RouteCollection $routes) use ($container) : RouteCollection {
            $routes->addCollection(
                $container['route.collection']
            );
            return $routes;
        };
    }

    /**
     * @return \Closure
     */
    private function getRouteCollection() : \Closure
    {
        return function (Container $container) : RouteCollection {
            return $container['file.loader']->load('Routes.yml');
        };
    }

    /**
     * @return \Closure
     */
    private function getFileLoader() : \Closure
    {
        return function (Container $container) : FileLoader {
            return new YamlFileLoader($container['config.location']);
        };
    }

    /**
     * @return \Closure
     */
    private function getConfigLocation() : \Closure
    {
        return function () : FileLocator {
            return new FileLocator(getenv('CONFIG_FOLDER'));
        };
    }
}
