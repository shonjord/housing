<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Housing\Domain\Partner\Service\PartnerOrderedHotelService;

/**
 * Registers all partner services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
class PartnerService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['sorted.partner_name'] = $this->getSortableWith('name');
        $container['sorted.partner_url']  = $this->getSortableWith('url');
    }

    /**
     * @param string $comparer
     * @return \Closure
     */
    private function getSortableWith(string $comparer) : \Closure
    {
        return function (Container $container) use ($comparer) : HotelServiceInterface {
            return new PartnerOrderedHotelService(
                $container['repository.hotel'],
                $container['service.cache'],
                $container["partner.{$comparer}"]
            );
        };
    }
}
