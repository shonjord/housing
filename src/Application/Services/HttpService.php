<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Infrastructure\Http\Request;
use Housing\Infrastructure\Http\ResponseHandler;
use Housing\Infrastructure\Http\RequestInterface;
use Housing\Infrastructure\Http\ResponseHandlerInterface;

/**
 * Registers all http services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HttpService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['service.request']  = $this->getRequest();
        $container['service.response'] = $this->getResponseHandler();
    }

    /**
     * @return \Closure
     */
    public function getRequest() : \Closure
    {
        return function (Container $container) : RequestInterface {
            return new Request($container['request_stack']);
        };
    }

    /**
     * @return \Closure
     */
    public function getResponseHandler() : \Closure
    {
        return function (Container $container) : ResponseHandlerInterface {
            return new ResponseHandler($container['serializer']);
        };
    }
}
