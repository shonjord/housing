<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Comparer\HotelNameComparer;
use Housing\Domain\Hotel\Comparer\HotelAddressComparer;

/**
 * Registers all hotel comparer services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
class HotelComparerService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['hotel.name']    = $this->getNameComparer();
        $container['hotel.address'] = $this->getAddressComparer();
    }

    /**
     * @return \Closure
     */
    private function getNameComparer() : \Closure
    {
        return function (Container $container) : HotelNameComparer {
            return new HotelNameComparer($container['comparer']);
        };
    }

    /**
     * @return \Closure
     */
    private function getAddressComparer() : \Closure
    {
        return function (Container $container) : HotelAddressComparer {
            return new HotelAddressComparer($container['comparer']);
        };
    }
}
