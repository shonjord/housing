<?php declare(strict_types=1);

namespace Housing\Application\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Housing\Domain\Hotel\Infrastructure\HotelRepository;
use Housing\Domain\Catalog\Infrastructure\CatalogRepository;
use Housing\Domain\Hotel\Repository\HotelRepositoryInterface;
use Housing\Domain\Catalog\Repository\CatalogRepositoryInterface;

/**
 * Registers all repository services on the given container.
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class RepositoryService implements ServiceProviderInterface
{
    /**
     * @param Container $container
     * @return void
     */
    public function register(Container $container) : void
    {
        $container['repository.catalog'] = $this->getCatalogRepository();
        $container['repository.hotel']   = $this->getHotelRepository();
    }

    /**
     * @return \Closure
     */
    private function getCatalogRepository() : \Closure
    {
        return function (Container $container) : CatalogRepositoryInterface {
            return new CatalogRepository($container['reader'], $container['serializer']);
        };
    }

    /**
     * @return \Closure
     */
    private function getHotelRepository() : \Closure
    {
        return function (Container $container) : HotelRepositoryInterface {
            return new HotelRepository($container['reader'], $container['serializer']);
        };
    }
}
