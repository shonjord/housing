<?php declare(strict_types=1);

namespace Housing\Application\Cache;

use Collections\MapInterface;

interface CacheServiceInterface
{
    /**
     * @param string $key
     * @return bool
     */
    public function isCached(string $key) : bool;

    /**
     * @param string $key
     * @param string $data
     * @return CacheServiceInterface
     */
    public function cacheString(string $key, string $data) : CacheServiceInterface;

    /**
     * @param string $key
     * @param MapInterface $collection
     * @return CacheServiceInterface
     */
    public function cacheCollection(string $key, MapInterface $collection) : CacheServiceInterface;

    /**
     * @param string $key
     * @return string
     */
    public function getString(string $key) : string;

    /**
     * @param string $key
     * @return array
     */
    public function getArray(string $key) : array;

    /**
     * @param string $key
     * @return MapInterface
     */
    public function getCollection(string $key) : MapInterface;
}
