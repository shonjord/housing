<?php declare(strict_types=1);

namespace Housing\Application\Cache;

use Predis\Client;
use Collections\Map;
use Collections\MapInterface;
use Housing\Infrastructure\Serializer\SerializerInterface;

final class CacheService implements CacheServiceInterface
{
    /**
     * @var Client
     */
    private $redis;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param Client $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(Client $redis, SerializerInterface $serializer)
    {
        $this->redis      = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function isCached(string $key): bool
    {
        return 1 === $this->redis->exists($key);
    }

    /**
     * @param string $key
     * @param string $data
     * @return CacheServiceInterface
     */
    public function cacheString(string $key, string $data) : CacheServiceInterface
    {
        $this->redis->set($key, $data);

        return $this;
    }

    /**
     * @param string $key
     * @param MapInterface $collection
     * @return CacheServiceInterface
     */
    public function cacheCollection(string $key, MapInterface $collection) : CacheServiceInterface
    {
        $this->cacheString($key, $this->serializer->serialize($collection));

        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getString(string $key) : string
    {
        return $this->redis->get($key);
    }

    /**
     * @param string $key
     * @return array
     */
    public function getArray(string $key) : array
    {
        return json_decode($this->getString($key), true);
    }

    /**
     * @param string $key
     * @return MapInterface
     */
    public function getCollection(string $key) : MapInterface
    {
        return Map::fromArray($this->getArray($key));
    }
}
