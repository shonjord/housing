
NO_COLOR    =\033[0m
OK_COLOR    =\033[32;01m
ERROR_COLOR =\033[31;01m
WARN_COLOR  =\033[33;01m

install:
	@if grep -q Housing /etc/hosts;\
	then echo "$(ERROR_COLOR)==> Application is already installed $(NO_COLOR)";\
	else make install_app;\
	fi;

install_app:
	@echo "$(WARN_COLOR)==> Installing the Application $(WARN_COLOR)"
	@echo "$(OK_COLOR)==> Registering new entry in the hosts $(NO_COLOR)"
	@echo "" | sudo tee -a /etc/hosts
	@echo "# Housing Application" | sudo tee -a /etc/hosts
	@echo "127.0.0.1 housing.dev" | sudo tee -a /etc/hosts
	@make start

start:
	@echo "$(OK_COLOR)==> Initializing the Application $(NO_COLOR)"
	@echo "$(OK_COLOR)==> Creating App-Sync volume $(NO_COLOR)"
	@docker volume create --name=app-sync
	@echo "$(OK_COLOR)==> Initializing containers $(NO_COLOR)"
	@docker-compose up -d
	@echo "$(OK_COLOR)==> Starting App-Sync $(NO_COLOR)"
	@docker-sync start
	@echo "$(OK_COLOR)==> Installing dependencies if there are any $(NO_COLOR)"
	@docker exec -it housing composer install

stop:
	@echo "$(OK_COLOR)==> Shutting down the Application $(NO_COLOR)"
	@docker-compose down
	@docker-sync stop
