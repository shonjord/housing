# Housing Application

This Application has an average of 60 ms of response

## PHP project practice
This practice follows areas of PHP development:

* OOP implementation, including interfaces, design patterns and SOLID principle.
* DDD Architectural
* Unit, integration, testing
* Namespaces, Closures/Anonymous functions, type hinting
* Reading resources from a local file system location
* Coping with JSON as data format

## Technology Stack

* PHP 7 - Alpine
* Composer
* PSR-2 and PSR-4 compatibility
* PHPUnit V 6
* Nginx
* Symfony components (HTTP Foundation, pimple, routing)
* JMS Serializer
* Whoops
* Docker
* Redis

## Installation
* Download and install Docker
* Run `make install` on your unix terminal, this process will also initialize the Application

## Initialize the Application
* Run `make start` on your unix terminal

## Stop the Application
* Run `make stop` on your unix terminal

Available cities for this practice:

* Düsseldorf | 15475
* Berlin     | 15476

## Current available endpoints for this practice

* `housing.dev/{city}`
* `housing.dev/{cityID}`
* `housing.dev/{city or cityID}?sorted=hotel_name`
* `housing.dev/{city or cityID}?sorted=hotel_address`
* `housing.dev/{city or cityID}?sorted=partner_name`
* `housing.dev/{city or cityID}?sorted=partner_url`
* `housing.dev/{city or cityID}?sorted=price_amount`

## Idea

The implementation must read the result from the data source and pass the values from the json file into the corresponding classes from the Entity namespace.

The entities of this practice encapsulate each other:

(Hotel) -[hasMany]-> (Partner) -[hasMany]-> (Price)

The JSON file has a similar but not equal structure.

## Implementation of Partner's URL validation

This practice builds a validation mechanism for the Partner Entity's homepage property using a value object.

## Implementations of the HotelServiceInterface

There are some implementations of HotelServiceInterface, the current ones are:

* Ordered by hotel name
* Ordered by hotel address
* Ordered by partner name
* Ordered by partner URL
* Ordered by price amount (ASC)

## Tests

To run the tests, simply enter into the Application container and run `phpunit`
