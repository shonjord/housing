<?php declare(strict_types=1);

/* @author Albert Agelviz <aagelviz@gmail.com>
|--------------------------------------------------------------------------
| Autoload requirement
|--------------------------------------------------------------------------
|
| This step will autoload the vendor folder, and require all the dependencies
| of this application.
|
*/
require_once __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Application Runner
|--------------------------------------------------------------------------
|
| This step runs the entire application
|
*/
(new \Housing\Application\Application)->run();
