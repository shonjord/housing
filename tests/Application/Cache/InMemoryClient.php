<?php declare(strict_types=1);

namespace Test\Application\Cache;

use Predis\Client;
use Collections\Map;

final class InMemoryClient extends Client
{
    /**
     * @var Map
     */
    private $data;

    /**
     * @param null $parameters
     * @param null $options
     */
    public function __construct($parameters = null, $options = null)
    {
        $this->data = new Map;
        parent::__construct($parameters, $options);
    }

    /**
     * @param $key
     * @return int
     */
    public function exists($key) : int
    {
        return (int) $this->data->containsKey($key);
    }

    /**
     * @param $key
     * @return string
     */
    public function get($key) : string
    {
        return (string) $this->data->get($key);
    }

    /**
     * @param $key
     * @param $value
     * @param null $expireResolution
     * @param null $expireTTL
     * @param null $flag
     * @return void
     */
    public function set($key, $value, $expireResolution = null, $expireTTL = null, $flag = null)
    {
        $this->data->set($key, $value);
    }
}
