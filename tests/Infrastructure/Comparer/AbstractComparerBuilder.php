<?php declare(strict_types=1);

namespace Test\Infrastructure\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Infrastructure\Comparer\MathematicalComparison;
use Housing\Infrastructure\Comparer\MathematicalComparisonInterface;

abstract class AbstractComparerBuilder
{
    /**
     * @var MathematicalComparison
     */
    protected $comparer;

    public function __construct()
    {
        $this->comparer = new MathematicalComparison;
    }

    /**
     * @param MathematicalComparisonInterface $comparer
     * @return AbstractComparerBuilder
     */
    public function withComparer(MathematicalComparisonInterface $comparer) : AbstractComparerBuilder
    {
        $this->comparer = $comparer;

        return $this;
    }

    /**
     * @return ComparerInterface
     */
    abstract public function build() : ComparerInterface;

    /**
     * @return ComparerInterface
     */
    abstract public static function buildDefault() : ComparerInterface;
}
