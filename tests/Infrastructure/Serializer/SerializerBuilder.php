<?php declare(strict_types=1);

namespace Test\Infrastructure\Serializer;

use Housing\Infrastructure\Serializer\Serializer;
use Housing\Infrastructure\Serializer\SerializerInterface;

/**
 * Returns a Serializer with its basic dependencies
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class SerializerBuilder
{
    /**
     * @return SerializerInterface
     */
    public static function build() : SerializerInterface
    {
        return new Serializer;
    }
}
