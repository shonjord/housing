<?php declare(strict_types=1);

namespace Test\Infrastructure\Repository;

use Collections\Generic\ComparerInterface;
use Test\Infrastructure\Reader\ReaderBuilder;
use Housing\Infrastructure\Reader\ReaderInterface;
use Test\Infrastructure\Serializer\SerializerBuilder;
use Housing\Infrastructure\Repository\AbstractRepository;

/**
 * Responsible to return data in memory
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class InMemoryAbstractRepository extends AbstractRepository
{
    /**
     * @var ReaderInterface
     */
    protected $reader;

    /**
     * @var ComparerInterface
     */
    protected $comparer;

    public function __construct()
    {
        parent::__construct(ReaderBuilder::buildDefault(), SerializerBuilder::build());
    }
}
