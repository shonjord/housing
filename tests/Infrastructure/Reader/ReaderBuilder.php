<?php declare(strict_types=1);

namespace Test\Infrastructure\Reader;

use Housing\Infrastructure\Reader\Reader;
use Housing\Infrastructure\Reader\ReaderInterface;

/**
 * Returns a Reader with its basic dependencies
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class ReaderBuilder
{
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        $this->file = getenv('DATA_TEST');
    }

    /**
     * @param string $file
     * @return ReaderBuilder
     */
    public function withFile(string $file) : ReaderBuilder
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return ReaderInterface
     */
    public function build() : ReaderInterface
    {
        return new Reader($this->file);
    }

    /**
     * @return ReaderInterface
     */
    public static function buildDefault() : ReaderInterface
    {
        return (new self)->build();
    }
}
