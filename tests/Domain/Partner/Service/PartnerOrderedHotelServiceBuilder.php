<?php declare(strict_types=1);

namespace Test\Domain\Partner\Service;

use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Test\Domain\Hotel\Service\AbstractHotelServiceBuilder;
use Test\Domain\Partner\Comparer\PartnerNameComparerBuilder;
use Housing\Domain\Partner\Service\PartnerOrderedHotelService;

/**
 * Builds PartnerOrderedHotelService
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerOrderedHotelServiceBuilder extends AbstractHotelServiceBuilder
{
    /**
     * @return HotelServiceInterface
     */
    public function build() : HotelServiceInterface
    {
        return new PartnerOrderedHotelService($this->repository, $this->cacheService, $this->comparer);
    }

    /**
     * @return HotelServiceInterface
     */
    public static function buildDefault() : HotelServiceInterface
    {
        return (new self)->withComparer(PartnerNameComparerBuilder::buildDefault())->build();
    }
}
