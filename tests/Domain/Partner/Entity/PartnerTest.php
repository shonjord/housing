<?php declare(strict_types=1);

namespace Test\Domain\Partner\Domain;

use Collections\Map;
use Collections\MapInterface;
use PHPUnit\Framework\TestCase;
use Housing\Domain\Partner\Entity\Partner;
use Housing\Domain\Partner\ValueObject\Url;

/**
 * Tests the basics of Partner entity
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerTest extends TestCase
{
    /**
     * @param string $name
     * @param Url $url
     * @param MapInterface $prices
     * @return void
     * @test
     * @dataProvider hotelDataProvider
     */
    public function itShouldGetCorrectInformation(string $name, Url $url, MapInterface $prices) : void
    {
        $partner = new Partner($name, $url, $prices);

        $this->assertEquals($name, $partner->getName());
        $this->assertEquals($url, $partner->getUrl());
        $this->assertEquals($prices, $partner->getPrices());
    }

    /**
     * @return void
     * @test
     * @expectedException \Housing\Domain\Partner\Exception\BadUrlException
     */
    public function itShouldThrowExceptionForBadUrlProvided() : void
    {
        new Partner('someFakePartner', new Url('http://this-website-does-not-exist.com/'), new Map);
    }

    /**
     * @return array
     */
    public function hotelDataProvider() : array
    {
        return [
            ['Booking', new Url('http://www.booking.com'), new Map],
            ['Hotels', new Url('http://www.hotels.es'),new Map],
        ];
    }
}
