<?php declare(strict_types=1);

namespace Test\Domain\Partner\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Domain\Partner\Comparer\PartnerUrlComparer;
use Test\Infrastructure\Comparer\AbstractComparerBuilder;

/**
 * Builds PartnerUrlComparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerUrlComparerBuilder extends AbstractComparerBuilder
{
    /**
     * @return ComparerInterface
     */
    public function build() : ComparerInterface
    {
        return new PartnerUrlComparer($this->comparer);
    }

    /**
     * @return ComparerInterface
     */
    public static function buildDefault() : ComparerInterface
    {
        return (new self)->build();
    }
}
