<?php declare(strict_types=1);

namespace Test\Domain\Partner\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Domain\Partner\Comparer\PartnerNameComparer;
use Test\Infrastructure\Comparer\AbstractComparerBuilder;

/**
 * Builds PartnerNameComparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PartnerNameComparerBuilder extends AbstractComparerBuilder
{
    /**
     * @return ComparerInterface
     */
    public function build(): ComparerInterface
    {
        return new PartnerNameComparer($this->comparer);
    }

    /**
     * @return ComparerInterface
     */
    public static function buildDefault(): ComparerInterface
    {
        return (new self)->build();
    }
}
