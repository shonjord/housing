<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Entity;

use Collections\Map;
use Collections\MapInterface;
use PHPUnit\Framework\TestCase;
use Housing\Domain\Hotel\Entity\Hotel;

/**
 * Tests the basics of Hotel entity
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelTest extends TestCase
{
    /**
     * @param string $name
     * @param string $city
     * @param MapInterface $partners
     * @dataProvider hotelDataProvider
     * @return void
     * @test
     */
    public function itShouldGetCorrectInformation(string $name, string $city, MapInterface $partners) : void
    {
        $hotel = new Hotel($name, $city, $partners);

        $this->assertEquals($name, $hotel->getName());
        $this->assertEquals($city, $hotel->getAddress());
        $this->assertEquals($partners, $hotel->getPartners());
    }

    /**
     * @return array
     */
    public function hotelDataProvider() : array
    {
        return [
            ['Hilton Berlin', 'Berlin', new Map],
            ['NH Berlin', 'Berlin', new Map]
        ];
    }
}
