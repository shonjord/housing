<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Domain;

use Collections\MapInterface;
use PHPUnit\Framework\TestCase;
use Test\Infrastructure\Serializer\SerializerBuilder;
use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Test\Domain\Hotel\Service\OrderedHotelServiceBuilder;
use Test\Domain\Hotel\Comparer\HotelAddressComparerBuilder;
use Test\Domain\Hotel\Service\UnorderedHotelServiceBuilder;
use Test\Domain\Partner\Comparer\PartnerUrlComparerBuilder;
use Test\Domain\Price\Service\PriceOrderedHotelServiceBuilder;
use Test\Domain\Partner\Service\PartnerOrderedHotelServiceBuilder;

/**
 * Responsible to test different scenarios of sorting hotels
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelIntegrationTest extends TestCase
{
    /**
     * @param HotelServiceInterface $service
     * @param string $expected
     * @return void
     * @dataProvider serviceProvider
     * @test
     */
    public function itShouldSortHotel(HotelServiceInterface $service, string $expected) : void
    {
        $this->assertEquals(
            $this->serialize($service->getResultForCity('berlin')),
            json_encode(json_decode($expected))
        );

        $this->assertEquals(
            $this->serialize($service->getResultForCity('15476')),
            json_encode(json_decode($expected))
        );
    }

    /**
     * @return array
     */
    public function serviceProvider() : array
    {
        return [
            [
                OrderedHotelServiceBuilder::buildDefault(),
                $this->hotelOrderedByName()
            ],
            [
                (new OrderedHotelServiceBuilder)->withComparer(HotelAddressComparerBuilder::buildDefault())->build(),
                $this->hotelOrderedByAddress()
            ],
            [
                UnorderedHotelServiceBuilder::buildDefault(),
                $this->unorderedHotel()
            ],
            [
                PartnerOrderedHotelServiceBuilder::buildDefault(),
                $this->partnerOrderedByName()
            ],
            [
                (new PartnerOrderedHotelServiceBuilder)
                    ->withComparer(PartnerUrlComparerBuilder::buildDefault())->build(),
                $this->partnerOrderedByUrl()
            ],
            [
                PriceOrderedHotelServiceBuilder::buildDefault(),
                $this->priceOrderedByAmount()
            ]
        ];
    }

    /**
     * @param MapInterface $hotels
     * @return string
     */
    private function serialize(MapInterface $hotels) : string
    {
        return SerializerBuilder::build()->serialize($hotels);
    }

    /**
     * @return string
     */
    private function hotelOrderedByName() : string
    {
        return <<<JSON
        [
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }

    /**
     * @return string
     */
    private function hotelOrderedByAddress() : string
    {
        return <<<JSON
        [
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }

    /**
     * @return string
     */
    private function unorderedHotel() : string
    {
        return <<<JSON
        [
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }

    /**
     * @return string
     */
    private function partnerOrderedByName() : string
    {
        return <<<JSON
        [
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }

    /**
     * @return string
     */
    private function partnerOrderedByUrl() : string
    {
        return <<<JSON
        [
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }

    /**
     * @return string
     */
    private function priceOrderedByAmount() : string
    {
        return <<<JSON
        [
           {
              "name":"Avila Berlin",
              "address":"Deorg-Glock-Stra\u00dfe 32, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Double Room",
                          "amount":5,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Double Room",
                          "amount":110,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Hilton Berlin",
              "address":"Ceorg-Glock-Stra\u00dfe 20, 40474 Berlin",
              "partners":[
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":10,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Single Room",
                          "amount":300,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           },
           {
              "name":"Berlin Mercure",
              "address":"Aeorg-Glock-Stra\u00dfe 19, 40475 Berlin",
              "partners":[
                 {
                    "name":"Hotels.com",
                    "url":"http:\/\/www.hotels.com",
                    "prices":[
                       {
                          "description":"Double Room",
                          "amount":45,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Single Room",
                          "amount":324,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 },
                 {
                    "name":"Booking.com",
                    "url":"http:\/\/www.booking.com",
                    "prices":[
                       {
                          "description":"Single Room",
                          "amount":125,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       },
                       {
                          "description":"Double Room",
                          "amount":139,
                          "from":"12-10-2012",
                          "to":"13-10-2012"
                       }
                    ]
                 }
              ]
           }
        ]
JSON;
    }
}
