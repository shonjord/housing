<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Repository;

use Collections\Map;
use Housing\Domain\Hotel\Repository\HotelRepositoryInterface;
use Test\Infrastructure\Repository\InMemoryAbstractRepository;

/**
 * Finds Hotels based on criteria
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
class InMemoryHotelRepository extends InMemoryAbstractRepository implements HotelRepositoryInterface
{
    /**
     * @param string $city
     * @return Map
     */
    public function findByCity(string $city) : Map
    {
        return $this->findBy($city)->getHotels();
    }
}
