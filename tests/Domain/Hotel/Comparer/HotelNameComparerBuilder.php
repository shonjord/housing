<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Domain\Hotel\Comparer\HotelNameComparer;
use Test\Infrastructure\Comparer\AbstractComparerBuilder;

/**
 * Builds HotelNameComparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelNameComparerBuilder extends AbstractComparerBuilder
{
    /**
     * @return ComparerInterface
     */
    public function build() : ComparerInterface
    {
        return new HotelNameComparer($this->comparer);
    }

    /**
     * @return ComparerInterface
     */
    public static function buildDefault() : ComparerInterface
    {
        return (new self)->build();
    }
}
