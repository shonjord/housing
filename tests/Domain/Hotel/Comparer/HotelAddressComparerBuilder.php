<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Domain\Hotel\Comparer\HotelAddressComparer;
use Test\Infrastructure\Comparer\AbstractComparerBuilder;

/**
 * Builds HotelAddressComparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class HotelAddressComparerBuilder extends AbstractComparerBuilder
{
    /**
     * @return ComparerInterface
     */
    public function build() : ComparerInterface
    {
        return new HotelAddressComparer($this->comparer);
    }

    /**
     * @return ComparerInterface
     */
    public static function buildDefault() : ComparerInterface
    {
        return (new self)->build();
    }
}
