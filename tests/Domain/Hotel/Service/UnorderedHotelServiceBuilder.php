<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Service;

use Housing\Domain\Hotel\Service\UnorderedHotelService;
use Housing\Domain\Hotel\Service\HotelServiceInterface;

/**
 * Builds UnorderedHotelService
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class UnorderedHotelServiceBuilder extends AbstractHotelServiceBuilder
{
    /**
     * @return HotelServiceInterface
     */
    public function build() : HotelServiceInterface
    {
        return new UnorderedHotelService($this->repository, $this->cacheService, $this->comparer);
    }

    /**
     * @return HotelServiceInterface
     */
    public static function buildDefault() : HotelServiceInterface
    {
        return (new self)->withComparer(null)->build();
    }
}
