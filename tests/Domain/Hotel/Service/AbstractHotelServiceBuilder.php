<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Service;

use Housing\Infrastructure\Reader\Reader;
use Collections\Generic\ComparerInterface;
use Test\Application\Cache\InMemoryCacheService;
use Housing\Domain\Hotel\Comparer\HotelNameComparer;
use Housing\Application\Cache\CacheServiceInterface;
use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Test\Domain\Hotel\Comparer\HotelNameComparerBuilder;
use Test\Domain\Hotel\Repository\InMemoryHotelRepository;
use Housing\Domain\Hotel\Repository\HotelRepositoryInterface;
use Housing\Domain\Hotel\Service\AbstractSortableHotelService;

/**
 * All classes who inherit from this, will have access to the basics dependencies
 * to build a HotelService
 *
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
abstract class AbstractHotelServiceBuilder
{
    /**
     * @var Reader
     */
    protected $repository;

    /**
     * @var HotelNameComparer
     */
    protected $comparer;

    /**
     * @var CacheServiceInterface
     */
    protected $cacheService;

    public function __construct()
    {
        $this->comparer     = HotelNameComparerBuilder::buildDefault();
        $this->cacheService = new InMemoryCacheService;
        $this->repository   = new InMemoryHotelRepository;
    }

    /**
     * @param ComparerInterface|null $comparer
     * @return AbstractHotelServiceBuilder
     */
    public function withComparer(?ComparerInterface $comparer) : AbstractHotelServiceBuilder
    {
        $this->comparer = $comparer;

        return $this;
    }

    /**
     * @param HotelRepositoryInterface $repository
     * @return AbstractHotelServiceBuilder
     */
    public function withRepository(HotelRepositoryInterface $repository) : AbstractHotelServiceBuilder
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * @param CacheServiceInterface $cacheService
     * @return AbstractSortableHotelService
     */
    public function withCacheService(CacheServiceInterface $cacheService) : AbstractSortableHotelService
    {
        $this->cacheService = $cacheService;

        return $this;
    }

    /**
     * @return HotelServiceInterface
     */
    abstract public function build() : HotelServiceInterface;

    /**
     * @return HotelServiceInterface
     */
    abstract public static function buildDefault() : HotelServiceInterface;
}
