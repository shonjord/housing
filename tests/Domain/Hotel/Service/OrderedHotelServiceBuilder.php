<?php declare(strict_types=1);

namespace Test\Domain\Hotel\Service;

use Housing\Domain\Hotel\Service\OrderedHotelService;
use Housing\Domain\Hotel\Service\HotelServiceInterface;

/**
 * Builds OrderedHotelService
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class OrderedHotelServiceBuilder extends AbstractHotelServiceBuilder
{
    /**
     * @return HotelServiceInterface
     */
    public function build() : HotelServiceInterface
    {
        return new OrderedHotelService($this->repository, $this->cacheService, $this->comparer);
    }

    /**
     * @return HotelServiceInterface
     */
    public static function buildDefault() : HotelServiceInterface
    {
        return (new self)->build();
    }
}
