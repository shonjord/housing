<?php declare(strict_types=1);

namespace Test\Domain\Price\Service;

use Housing\Domain\Hotel\Service\HotelServiceInterface;
use Housing\Domain\Price\Service\PriceOrderedHotelService;
use Test\Domain\Price\Comparer\PriceAmountComparerBuilder;
use Test\Domain\Hotel\Service\AbstractHotelServiceBuilder;

/**
 * Builds PriceOrderedHotelService
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceOrderedHotelServiceBuilder extends AbstractHotelServiceBuilder
{
    /**
     * @return HotelServiceInterface
     */
    public function build() : HotelServiceInterface
    {
        return new PriceOrderedHotelService($this->repository, $this->cacheService, $this->comparer);
    }

    /**
     * @return HotelServiceInterface
     */
    public static function buildDefault() : HotelServiceInterface
    {
        return (new self)->withComparer(PriceAmountComparerBuilder::buildDefault())->build();
    }
}
