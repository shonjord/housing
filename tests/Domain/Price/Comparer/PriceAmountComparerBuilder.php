<?php declare(strict_types=1);

namespace Test\Domain\Price\Comparer;

use Collections\Generic\ComparerInterface;
use Housing\Domain\Price\Comparer\PriceAmountComparer;
use Test\Infrastructure\Comparer\AbstractComparerBuilder;

/**
 * Builds PriceAmountComparer
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class PriceAmountComparerBuilder extends AbstractComparerBuilder
{
    /**
     * @return ComparerInterface
     */
    public function build() : ComparerInterface
    {
        return new PriceAmountComparer($this->comparer);
    }

    /**
     * @return ComparerInterface
     */
    public static function buildDefault() : ComparerInterface
    {
        return (new self)->build();
    }
}
